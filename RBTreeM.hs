module RBTreeM (
    RBTree,
    Color,
    Value,
    fromList, 
    insert, 
    member,
    find,
    union, 
    getHeight,
    getElements,
    getKeys, 
    toList, 
    isBalanced
    ) where
{--structures--}
    data Value t = 
          None
        | Value t
        deriving (Show)

    data Color = 
          Red 
        | Black 
        deriving (Eq)   

    data RBTree key val = 
          Leaf 
        | Node !Color !key !val !(RBTree key val) !(RBTree key val) 
{--isBalanced--}
    isBalanced :: RBTree key val -> Bool
    isBalanced t = isBlackSame t && isRedSeparate t

    isBlackSame :: RBTree key val -> Bool
    isBlackSame t = all (n==) ns
      where
        n:ns = blacks t

    blacks :: RBTree key val -> [Int]
    blacks = blacks' 0
      where
        blacks' n Leaf = [n+1]
        blacks' n (Node Red _ _ l r) = blacks' n  l ++ blacks' n  r
        blacks' n (Node Black _ _ l r) = blacks' n' l ++ blacks' n' r
          where
            n' = n + 1

    isRedSeparate :: RBTree key val -> Bool
    isRedSeparate = reds Black

    reds :: Color -> RBTree key val -> Bool
    reds _ Leaf = True
    reds Red (Node Red _ _ _ _) = False
    reds _ (Node c _ _ l r) = reds c l && reds c r
{--getColor--}
    getColor :: RBTree key val -> Color
    getColor (Node col _ _ _ _) = col
    getColor Leaf = Black
{--setRed--}
    setRed :: RBTree key val -> RBTree key val
    setRed (Node _ key val lt rt) = Node Red key val lt rt
    setRed Leaf = Leaf
{--setBlack--}
    setBlack :: RBTree key val -> RBTree key val
    setBlack (Node _ key val lt rt) = Node Black key val lt rt
    setBlack Leaf = Leaf
{--getEmpty--}
    getEmpty :: RBTree key val
    getEmpty = Leaf
{--getSingleton--}
    getSingleton :: (Ord key) => (key, val) -> RBTree key val
    getSingleton (key, val) = Node Black key val Leaf Leaf
{--member--}
    member :: (Ord key) => key -> RBTree key val -> Bool
    member key Leaf = False
    member key (Node _ tkey _ lt rt) = case compare key tkey of
        LT -> member key lt
        GT -> member key rt
        EQ -> True
{--find--}
    find :: (Ord key) => key -> RBTree key val -> Value val
    find key Leaf = None
    find key (Node _ tkey val lt rt) = case compare key tkey of
        LT -> find key lt
        GT -> find key rt
        EQ -> Value val
{--insert--}
    insert :: (Ord key) => (key, val) -> RBTree key val -> RBTree key val
    insert (key, val) tree = (setBlack . insert') tree
        where 
            insert' Leaf = Node Red key val Leaf Leaf
            insert' (Node col tkey tval lt rt) = case compare key tkey of
                LT -> balance col tkey tval (insert' lt) rt
                GT -> balance col tkey tval lt (insert' rt)
                EQ -> Node col tkey val lt rt
{--balance--}
    balance :: Color -> key -> val -> RBTree key val -> RBTree key val -> RBTree key val
    
    balance Black zkey zval (Node Red ykey yval (Node Red xkey xval alt brt) crt) drt =
            Node Red ykey yval (Node Black xkey xval alt brt) (Node Black zkey zval crt drt)

    balance Black zkey zval (Node Red xkey xval alt (Node Red ykey yval brt crt)) drt =
            Node Red ykey yval (Node Black xkey xval alt brt) (Node Black zkey zval crt drt)

    balance Black xkey xval alt (Node Red zkey zval (Node Red ykey yval brt crt) drt) =
            Node Red ykey yval (Node Black xkey xval alt brt) (Node Black zkey zval crt drt)

    balance Black xkey xval alt (Node Red ykey yval brt (Node Red zkey zval crt drt)) =
            Node Red ykey yval (Node Black xkey xval alt brt) (Node Black zkey zval crt drt)

    balance col key val lt rt = 
        Node col key val lt rt

    balanceL :: key -> val -> RBTree key val -> RBTree key val -> RBTree key val
    balanceL ykey yval (Node Red xkey xval alt brt) crt = 
        Node Red ykey yval (Node Black xkey xval alt brt) crt

    balanceL xkey xval clt (Node Black ykey yval alt brt) =
        balance Black ykey yval clt (Node Red xkey xval alt brt)

    balanceL xkey xval clt (Node Red zkey zval (Node Black ykey yval alt brt) crt) =
        Node Red ykey yval (Node Black xkey xval clt alt) (balance Black zkey zval brt (setRed crt))

    balanceR ::key -> val -> RBTree key val -> RBTree key val -> RBTree key val
    balanceR xkey xval alt (Node Red ykey yval blt crt) = 
        Node Red xkey xval alt (Node Black ykey yval blt crt)
    
    balanceR ykey yval (Node Black xkey xval alt brt) crt = 
        balance Black ykey yval (Node Red xkey xval alt brt) crt

    balanceR zkey zval (Node Red xkey xval alt (Node Black ykey yval blt crt)) drt = 
        Node Red ykey yval (balance Black xkey xval (setRed alt) blt) (Node Black zkey zval crt drt)
{--fromList--}
    fromList :: (Ord key) => [(key, val)] -> RBTree key val
    fromList [] = Leaf     
    fromList [(key, val)] = getSingleton (key, val)
    fromList lst = insert (last lst) (fromList (init lst))
{--toList--}
    toList :: (Ord key) => RBTree key val -> [(key, val)]
    toList t = toList' t []
        where 
            toList' Leaf lst = lst
            toList' (Node _ key val lt rt) lst = toList' lt ((key, val) : toList' rt lst)
{--remove--}
    remove :: (Ord key) => key -> RBTree key val -> RBTree key val
    remove key tree = (setBlack . remove') tree
        where 
            remove' Leaf = Leaf
            remove' (Node _ tkey tval lt rt) = case compare key tkey of
                LT -> remL tkey tval lt rt
                GT -> remR tkey tval lt rt
                EQ -> append lt rt
            remL tkey tval lt@(Node Black _ _ _ _) rt = 
                balanceL tkey tval (remove' lt) rt
            remL tkey tval lt rt = 
                Node Red tkey tval (remove' lt) rt
            remR tkey tval lt rt@(Node Black _ _ _ _) = 
                balanceR tkey tval lt (remove' rt)
            remR tkey tval lt rt =
                Node Red tkey tval lt (remove' rt)
{--append--}
    append :: RBTree key val -> RBTree key val -> RBTree key val
    append Leaf t = t
    append t Leaf = t
    append (Node Red xkey xval a b) (Node Red ykey yval c d) =
        case append b c of 
            Node Red zkey zval b' c' -> Node Red zkey zval (Node Red xkey xval a b') (Node Red ykey yval c' d)
            bc -> Node Red xkey xval a (Node Red ykey yval bc d)
    append (Node Black xkey xval a b) (Node Black ykey yval c d) = 
        case append b c of
            Node Red zkey zval b' c' -> Node Red zkey zval (Node Black xkey xval a b') (Node Black ykey yval c' d)
            bc -> balanceL xkey xval a (Node Black ykey yval bc d)
    append at (Node Red xkey xval blt crt) = Node Red xkey xval (append at blt) crt
    append (Node Red xkey xval alt brt) ct = Node Red xkey xval alt (append brt ct)
{--union--}
    union :: (Ord key) => RBTree key val -> RBTree key val -> RBTree key val
    union Leaf t = t
    union t Leaf = t
    union t1 t2@(Node _ key2 val lt rt) = 
        union (union (insert (key2, val) t1) lt) rt
{--getKeys--}
    getKeys :: (Ord key) => RBTree key val -> [key]
    getKeys t = getKeys' t []
        where
            getKeys' Leaf lst = lst
            getKeys' (Node _ key _ lt rt) lst = getKeys' lt (key : getKeys' rt lst)
{--getElements--}
    getElements :: RBTree key val -> [val]
    getElements t = getElements' t []
        where
            getElements' Leaf lst = lst
            getElements' (Node _ _ val lt rt) lst = getElements' lt (val : getElements' rt lst)
{--getHeight--}
    getHeight :: RBTree key val -> Int 
    getHeight Leaf = 0
    getHeight (Node _ _ _ Leaf Leaf) = 1
    getHeight (Node _ _ _ lt Leaf) = 1 + getHeight lt
    getHeight (Node _ _ _ Leaf rt) = 1 + getHeight rt
    getHeight (Node _ _ _ lt rt) = 1 + max hlt hrt
        where
            hlt = getHeight lt
            hrt = getHeight rt
{--show--}
    instance Show Color where
        show Red = "R"
        show Black = "B"

    instance (Show key, Show val) => Show (RBTree key val) where
        show Leaf = "L"
        show t = "#" ++ rbtshow "" t
            where
                rbtshow pref Leaf = "L"
                rbtshow pref (Node col key val lt rt) = 
                    ("\n" ++ pref) ++ show key ++ "::" ++ show val ++ " (" ++ show col ++ ")" ++ "\n" ++
                    (pref ++ "|--->" ++ rbtshow (pref ++ "|     ") rt) ++ "\n" ++
                    (pref ++ "\\--->" ++ rbtshow (pref ++ "     ") lt)