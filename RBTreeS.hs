module RBTreeS (
    RBTree,
    Color,
    fromList, 
    insert, 
    member,
    union, 
    getHeight, 
    toList, 
    isBalanced
    ) where
{--structures--}
    data Color = 
          Red 
        | Black 
        deriving (Eq)   

    data RBTree t = 
          Leaf 
        | Node !Color !t !(RBTree t) !(RBTree t) 
{--isBalanced--}
    isBalanced :: RBTree a -> Bool
    isBalanced t = isBlackSame t && isRedSeparate t

    isBlackSame :: RBTree a -> Bool
    isBlackSame t = all (n==) ns
      where
        n:ns = blacks t

    blacks :: RBTree a -> [Int]
    blacks = blacks' 0
      where
        blacks' n Leaf = [n+1]
        blacks' n (Node Red _ l r) = blacks' n  l ++ blacks' n  r
        blacks' n (Node Black _ l r) = blacks' n' l ++ blacks' n' r
          where
            n' = n + 1

    isRedSeparate :: RBTree a -> Bool
    isRedSeparate = reds Black

    reds :: Color -> RBTree t -> Bool
    reds _ Leaf = True
    reds Red (Node Red _ _ _) = False
    reds _ (Node c _ l r) = reds c l && reds c r
{--getColor--}
    getColor :: RBTree t -> Color
    getColor (Node col _ _ _) = col
    getColor Leaf = Black
{--setRed--}
    setRed :: RBTree t -> RBTree t
    setRed (Node _ val lt rt) = Node Red val lt rt
    setRed Leaf = Leaf
{--setBlack--}
    setBlack :: RBTree t -> RBTree t
    setBlack (Node _ val lt rt) = Node Black val lt rt
    setBlack Leaf = Leaf
{--getEmpty--}
    getEmpty :: RBTree t
    getEmpty = Leaf
{--getSingleton--}
    getSingleton :: (Ord t) => t -> RBTree t
    getSingleton val = Node Black val Leaf Leaf
{--member--}
    member :: (Ord t) => t -> RBTree t -> Bool
    member val Leaf = False
    member val (Node _ tval lt rt) = case compare val tval of
        LT -> member val lt
        GT -> member val rt
        EQ -> True
{--insert--}
    insert :: (Ord t) => t -> RBTree t -> RBTree t
    insert val tree = setBlack $ insert' tree
        where 
            insert' Leaf = Node Red val Leaf Leaf
            insert' (Node col tval lt rt) = case compare val tval of
                LT -> balance col tval (insert' lt) rt
                GT -> balance col tval lt (insert' rt)
                EQ -> Node col tval lt rt
{--balance--}
    balance :: Color -> t -> RBTree t -> RBTree t -> RBTree t
    
    balance Black zval (Node Red yval (Node Red xval alt brt) crt) drt =
            Node Red yval (Node Black xval alt brt) (Node Black zval crt drt)

    balance Black zval (Node Red xval alt (Node Red yval brt crt)) drt =
            Node Red yval (Node Black xval alt brt) (Node Black zval crt drt)

    balance Black xval alt (Node Red zval (Node Red yval brt crt) drt) =
            Node Red yval (Node Black xval alt brt) (Node Black zval crt drt)

    balance Black xval alt (Node Red yval brt (Node Red zval crt drt)) =
            Node Red yval (Node Black xval alt brt) (Node Black zval crt drt)

    balance col val lt rt = 
        Node col val lt rt

    balanceL :: t -> RBTree t -> RBTree t -> RBTree t
    balanceL yval (Node Red xval alt brt) crt = 
        Node Red yval (Node Black xval alt brt) crt

    balanceL xval clt (Node Black yval alt brt) =
        balance Black yval clt (Node Red xval alt brt)

    balanceL xval clt (Node Red zval (Node Black yval alt brt) crt) =
        Node Red yval (Node Black xval clt alt) (balance Black zval brt (setRed crt))

    balanceR :: t -> RBTree t -> RBTree t -> RBTree t
    balanceR xval alt (Node Red yval blt crt) = 
        Node Red xval alt (Node Black yval blt crt)
    
    balanceR yval (Node Black xval alt brt) crt = 
        balance Black yval (Node Red xval alt brt) crt

    balanceR zval (Node Red xval alt (Node Black yval blt crt)) drt = 
        Node Red yval (balance Black xval (setRed alt) blt) (Node Black zval crt drt)
{--fromList--}
    fromList :: (Ord t) => [t] -> RBTree t
    fromList [] = Leaf     
    fromList [val] = getSingleton val
    fromList lst = insert (last lst) (fromList (init lst))
{--toList--}
    toList :: (Ord t) => RBTree t -> [t]
    toList t = toList' t []
        where 
            toList' Leaf lst = lst
            toList' (Node _ val lt rt) lst = toList' lt (val : toList' rt lst)
{--remove--}
    remove :: (Ord t) => t -> RBTree t -> RBTree t
    remove val tree = setBlack $ remove' tree
        where 
            remove' Leaf = Leaf
            remove' (Node _ tval lt rt) = case compare val tval of
                LT -> remL tval lt rt
                GT -> remR tval lt rt
                EQ -> append lt rt
            remL tval lt@(Node Black _ _ _) rt = 
                balanceL tval (remove' lt) rt
            remL tval lt rt = 
                Node Red tval (remove' lt) rt
            remR tval lt rt@(Node Black _ _ _) = 
                balanceR tval lt (remove' rt)
            remR tval lt rt =
                Node Red tval lt (remove' rt)
{--append--}
    append :: RBTree t -> RBTree t -> RBTree t
    append Leaf t = t
    append t Leaf = t
    append (Node Red x a b) (Node Red y c d) =
        case append b c of 
            Node Red z b' c' -> Node Red z (Node Red x a b') (Node Red y c' d)
            bc -> Node Red x a (Node Red y bc d)
    append (Node Black x a b) (Node Black y c d) = 
        case append b c of
            Node Red z b' c' -> Node Red z (Node Black x a b') (Node Black y c' d)
            bc -> balanceL x a (Node Black y bc d)
    append a (Node Red x b c) = Node Red x (append a b) c
    append (Node Red x a b) c = Node Red x a (append b c)
{--join--}
{--merge--}
{--union--}
    union :: (Ord key) => RBTree key -> RBTree key -> RBTree key
    union Leaf t = t
    union t Leaf = t
    union t1 t2@(Node _ key2 lt rt) = 
        union (union (insert key2 t1) lt) rt
{--intersection--}
{--difference--}
{--subset--}
{--getHeight--}
    getHeight :: RBTree t -> Int 
    getHeight Leaf = 0
    getHeight (Node _ _ Leaf Leaf) = 1
    getHeight (Node _ _ lt Leaf) = 1 + getHeight lt
    getHeight (Node _ _ Leaf rt) = 1 + getHeight rt
    getHeight (Node _ _ lt rt) = 1 + max hlt hrt
        where
            hlt = getHeight lt
            hrt = getHeight rt
{--show--}
    instance Show Color where
        show Red = "R"
        show Black = "B"

    instance (Show t) => Show (RBTree t) where
        show Leaf = "L"
        show t = "#" ++ rbtshow "" t
            where
            rbtshow pref Leaf = "L"
            rbtshow pref (Node col val lt rt) = 
                ("\n" ++ pref) ++ show val ++ " (" ++ show col ++ ")" ++ "\n" ++
                (pref ++ "|--->" ++ rbtshow (pref ++ "|     ") rt) ++ "\n" ++
                (pref ++ "\\--->" ++ rbtshow (pref ++ "     ") lt)